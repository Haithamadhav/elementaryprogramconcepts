#########################################################################################

atf.c

This program demonstrates the synchronization of threads using
atomic fetch add operation of gcc
refer: https://gcc.gnu.org/onlinedocs/gcc/_005f_005fatomic-Builtins.html

Built-in Function: type __atomic_add_fetch (type *ptr, type val, int memorder)

perform the operation suggested by the name, and return the result of the operation.
