#########################################################################################
	cas.c

This program is the demonstration of synchronization using
atomic compare and swap operation of gcc
refer https://gcc.gnu.org/onlinedocs/gcc-4.1.0/gcc/Atomic-Builtins.html

function cas(p : pointer to int, old : int, new : int) returns bool {
    if *p ≠ old {
        return false
    }
    *p ← new
    return true
}


bool __sync_bool_compare_and_swap (type *ptr, type oldval type newval, ...)

returns true if the comparison is successful and newval was written.

