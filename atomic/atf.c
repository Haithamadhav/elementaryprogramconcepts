#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
int sum = 0;
void* tFun(void * arg) {
	int *val = (int *)arg;
	int a = *val;
	__atomic_add_fetch(&sum, a, __ATOMIC_SEQ_CST);
	printf("\nThe sum is .. %d",sum);
}
int main(){
	pthread_t id[10];
	int arr[10] = {1,2,3,4,5,6,7,8,9,10};
	for (int i = 0; i<10; i++){
		pthread_create(&id[i], NULL, tFun, &arr[i]);
	}
	for(int j=0;j<10;j++){
		pthread_join(id[j],NULL);
	}
	printf("\nThe final value is %d", sum);
}
