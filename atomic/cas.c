#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
int sum = 0;
int lock = 1;
/*thread function definition*/
void* threadFunction(void * args)
{
	 while(1)
    	{
		while(__sync_bool_compare_and_swap(&lock,1,0) != 1);
		sleep(2);
		int *a = (int *)args;
		sum += *a;
        	printf("Sum is.. %d\n", sum);
		lock = 1;
    	}
}
int main()
{
    	/*creating thread id*/
    	pthread_t id, id2;
    	int ret;
    	int a=1, b=100;
    	/*creating thread*/
    	ret = pthread_create(&id,NULL,threadFunction,&a);
    	ret = pthread_create(&id2,NULL,threadFunction,&b);
	if(ret==0){
        	printf("Thread created successfully.\n");
		pthread_join(id,NULL);
		pthread_join(id2,NULL);
    	}
    	else{
        	printf("Thread not created.\n");
        	return 0; /*return from main*/
    	}
    	return 0;
}
