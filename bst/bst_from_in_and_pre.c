#include <stdio.h>
#include <malloc.h>
struct treenode {
    int data;
    struct treenode *left;
    struct treenode *right;
} *treehead;
struct listnode {
    int data;
    struct listnode *next;
    struct listnode *prev;
};
void in_order_traversal(struct treenode * root){
    if(root == NULL){
        return;
    } else {
        in_order_traversal(root->left);
        printf("%d ", root->data);
        in_order_traversal(root->right);
    }
}
int findInd(int I[], int starti, int endi, int value)
{
    int i;
    for(i = starti; i< endi; i++){
        if(I[i] == value)
        {
            return i;
        }
    }
}
int preorderindex = 0;
struct treenode* buildbinaryTree(int I[], int P[], int starti, int endi)
{
    if(starti>endi)
    {
        return NULL;
    } else {
    struct treenode* new = (struct treenode *)malloc(sizeof(struct treenode));
    new->data = P[preorderindex];
    new->left = NULL;
    new->right = NULL;
    preorderindex++;
    if(starti == endi){
        return new;
    } else {
    int indexIn  = findInd(I, starti, endi, new->data);
    new->left = buildbinaryTree(I, P, starti, indexIn-1);
    new->right = buildbinaryTree(I, P, indexIn+1, endi);
    }
    }
}

main()
{
    int I[100], P[100], size, i;
    printf("Enter number of nodes");
    scanf("%d", &size);
    printf("Enter inorder traversal");
    for(i = 0; i<size; i++)
    {
        scanf("%d", &I[i]);
    }
    printf("Enter preorder traversal");
    for(i = 0; i<size; i++)
    {
        scanf("%d", &P[i]);
    }
    struct treenode* root = buildbinaryTree(I, P, 0, size-1);
    printf("The inorder traversal is: \n");
    in_order_traversal(root);
}
