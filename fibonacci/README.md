Fibonacci series can be implemented recursively and using tail recursion.
Recursive fibonacci is more time consuming because it creates multiple calls with same arguments.
The call graph can be examined for the same.
The solution is tail recursion.
