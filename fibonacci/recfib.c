#include<stdio.h>
#include <time.h>   	// for clock_t, clock(), CLOCKS_PER_SEC
#include <unistd.h> 	// for sleep()
int f(int);
 
int main(int argc, char *argv[])
{
  double time_spent = 0.0;
  clock_t begin = clock();
  int k, s=1, n=0;
  /*for(k=1; k<argc;k++){
	printf("\n the value of argv is %c\n", *argv[k]);
  	n = n + (*argv[k]-48)*s;
	s = s*10;
  }*/
  //const std::string snum = argv[i];
  n = atoi(argv[1]);
  int i = 0, c;
  //printf("\nFibonacci series terms for %d are:\n", n);
  for (c = 1; c <= n; c++)
  {
    f(i);
    i++;
  }
  clock_t end = clock();
  time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
  printf("\nTime elpased is %f seconds\n", time_spent);
  return 0;
}
 
int f(int n)
{
  if (n == 0 || n == 1)
    return n;
  else
    return (f(n-1) + f(n-2));
}
