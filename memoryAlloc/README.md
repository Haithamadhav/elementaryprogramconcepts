Defining a structure should be taken care so that memory allocated is minimum. 

Because of padding in word size, structures with same variables declared in different order 
will consume different memory sizes. The demonstration is shown in the program.
