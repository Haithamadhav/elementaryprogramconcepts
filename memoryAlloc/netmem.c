#include <stdio.h>
#include <string.h>
 
struct student 
{
       	int id1;
        int id2;
        char a;
        char b;
        float percentage;
};
struct student2 {
       	int id1;
	char a;
	int id2;
	char b;
	float percentage;
};
int main() 
{
    int i;
    struct student record1 = {1, 2, 'A', 'B', 90.5};
    printf("size of structure in bytes : %ld\n", sizeof(struct student));
    printf("size of structure 2 in bytes : %ld\n", sizeof(struct student2));
    printf("\nAddress of id1        = %d", &record1.id1 );
    printf("\nAddress of id2        = %d", &record1.id2 );
    printf("\nAddress of a          = %d", &record1.a );
    printf("\nAddress of b          = %d", &record1.b );
    printf("\nAddress of percentage = %d",&record1.percentage);					 
    return 0;
}
