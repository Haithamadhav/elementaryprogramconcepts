#include <stdio.h>
#include <malloc.h>
#include <stddef.h>
#include <stdint.h>
int main()
{
	int *ptr,i;
	ssize_t x;
	size_t n = 10000000000;
	printf("\n the sizemax size is %zu", SIZE_MAX);
	ptr = (int *)malloc(n* sizeof(int));
	if(ptr != NULL)
	{
		printf("\nallocated %zu\n", n);
	}
	else {
		printf("\n Not allocated\n");
	}
}
