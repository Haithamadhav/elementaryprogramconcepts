#!/bin/sh
read line
set -- $line
a=$1
b=$2
echo '\\begin{table}[h]'>>out.txt
c='{|c'
d='|c'
e='|}'
one=1
while [ $one -lt $a ] 
do 
	c="$c$d"
	a=$(($a - $one))
	echo $a
done
ini='\\begin{tabular}'
echo "$ini$c$e">>out.txt 
while read -r line;do
	echo $line | sed -e 's/ /\&/g'>>out.txt
	echo '\\\\'>>out.txt
done
echo '\\end{tabular}'>>out.txt
echo '\\end{table}'>>out.txt
