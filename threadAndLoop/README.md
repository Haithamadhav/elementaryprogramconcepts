The program is run with inner loop threaded(parallelised) and outer loop threaded and both 
loops threaded. The execution time is as follows:
1. Both parallelised: 0.4 s approx.
2. Outer loop : 0.04 approx.
3. Inner loop : 0.3 s approx.

When inner loops are parallelised, the number of threads gets increased drastically
it will cause thread overhead and increase execution time.
