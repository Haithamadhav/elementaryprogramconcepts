#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include <time.h>
#include <unistd.h>
int sum = 0;
int k = 1;
void* tFun(void * arg){
	int *val = (int *)arg;
	int a = *val;
	__atomic_add_fetch(&sum,a,__ATOMIC_SEQ_CST);
}
void main(){
	double time_spent = 0.0;
  	clock_t begin = clock();
	pthread_t id[100];
	pthread_t id2[10000];
	int a = 1;
	for(int i=1;i<=100;i++){
//		pthread_create(&id[i],NULL,tFun,&a);
		sum=sum+a;
		for(int j=1;j<=100;j++){
			printf("\n The second id is %d \n", k);
			pthread_create(&id2[k],NULL,tFun,&a);
			k++;
			sum = sum+a;
		}
	}
	for(int m=1;m<=100;m++){
		pthread_join(id[m], NULL);
	}
	for(int l=1;l<=10000;l++){
		pthread_join(id2[l],NULL);
	}
	printf("\n The sum is %d",sum);
	clock_t end = clock();
  	time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
  	printf("\nTime elpased is %f seconds\n", time_spent);
}
